package amqp091_go_wrapper

import (
	"context"
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	"time"
)

func (br *Broker) Produce(message []byte, queueName string) error {
	q, err := br.channel.QueueDeclare(
		queueName,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		br.logger.Errorf("failed to declare a queue:%s", err)
		return fmt.Errorf("failed to declare a queue:%s", err)
	}
	br.channel.Confirm(false)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err = br.channel.PublishWithContext(ctx,
		"",
		q.Name,
		false,
		false,
		amqp.Publishing{
			DeliveryMode: 2,
			ContentType:  "application/json",
			Body:         message,
		})
	if err != nil {
		br.logger.Errorf("failed to publish a message:%s", err)
		return fmt.Errorf("failed to publish a message:%s", err)
	}

	return nil
}
