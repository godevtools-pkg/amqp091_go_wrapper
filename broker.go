package amqp091_go_wrapper

import (
	"errors"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/godevtools-pkg/logging"
	"sync"
	"time"
)

type Config amqp.Config

type Broker struct {
	logger              logging.Logger
	url                 string
	channel             *amqp.Channel
	connection          *amqp.Connection
	channelMux          *sync.RWMutex
	notifyCancelOrClose chan error
	reconnectInterval   time.Duration
	reconnectionCount   uint
}

func NewBroker(logger logging.Logger, brokerUrl string, reconnectInterval time.Duration) (*Broker, error) {
	amqpConn, err := amqp.Dial(brokerUrl)
	if err != nil {
		return nil, err
	}

	ch, err := amqpConn.Channel()
	if err != nil {
		return nil, err
	}

	broker := &Broker{
		logger:              logger,
		url:                 brokerUrl,
		channel:             ch,
		connection:          amqpConn,
		channelMux:          &sync.RWMutex{},
		notifyCancelOrClose: make(chan error),
		reconnectInterval:   reconnectInterval,
	}
	go broker.startNotifyCancelOrClosed()

	return broker, nil
}

func (br *Broker) startNotifyCancelOrClosed() {
	notifyCloseChan := br.channel.NotifyClose(make(chan *amqp.Error, 1))
	notifyCancelChan := br.channel.NotifyCancel(make(chan string, 1))
	select {
	case err := <-notifyCloseChan:
		if err != nil {
			br.logger.Errorf("attempting to reconnect to amqp server after close with error: %v", err)
			br.reconnectLoop()
			br.logger.Warnf("successfully reconnected to amqp server")
			br.notifyCancelOrClose <- err
		}
		if err == nil {
			br.logger.Infof("amqp channel closed gracefully")
		}
	case err := <-notifyCancelChan:
		br.logger.Errorf("attempting to reconnect to amqp server after cancel with error: %s", err)
		br.reconnectLoop()
		br.logger.Warnf("successfully reconnected to amqp server after cancel")
		br.notifyCancelOrClose <- errors.New(err)
	}
}

// reconnectLoop continuously attempts to reconnect
func (br *Broker) reconnectLoop() {
	for {
		if br.reconnectInterval == 0 {
			br.reconnectInterval = 5 * time.Second
		}

		br.logger.Infof("waiting %s seconds to attempt to reconnect to amqp server", br.reconnectInterval)
		time.Sleep(br.reconnectInterval)

		err := br.reconnect()
		if err != nil {
			br.logger.Errorf("error reconnecting to amqp server: %v", err)
		} else {
			br.reconnectionCount++
			go br.startNotifyCancelOrClosed()
			return
		}
	}
}

// reconnect safely closes the current channel and obtains a new one
func (br *Broker) reconnect() error {
	br.channelMux.Lock()
	defer br.channelMux.Unlock()

	amqpConn, err := amqp.Dial(br.url)
	if err != nil {
		return err
	}

	ch, err := amqpConn.Channel()
	if err != nil {
		return err
	}

	if err = br.channel.Close(); err != nil {
		br.logger.Warnf("error closing channel while reconnecting: %v", err)
	}

	if err = br.connection.Close(); err != nil {
		br.logger.Warnf("error closing connection while reconnecting: %v", err)
	}

	br.connection = amqpConn
	br.channel = ch

	return nil
}

// Close safely closes the current channel and connection
func (br *Broker) Close() error {
	br.channelMux.Lock()
	defer br.channelMux.Unlock()

	err := br.channel.Close()
	if err != nil {
		return err
	}

	err = br.connection.Close()
	if err != nil {
		return err
	}

	return nil
}
