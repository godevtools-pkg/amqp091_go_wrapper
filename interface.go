package amqp091_go_wrapper

type MsgProducer interface {
	Produce(message []byte, queueName string) error
}

type MsgHandler interface {
	Handle(message []byte) error
}
