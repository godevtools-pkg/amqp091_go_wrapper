package amqp091_go_wrapper

import (
	amqp "github.com/rabbitmq/amqp091-go"
)

type Consumer struct {
	broker      *Broker
	concurrency int
}

func NewConsumer(broker *Broker, concurrency int) *Consumer {
	return &Consumer{
		broker:      broker,
		concurrency: concurrency,
	}
}

func (c *Consumer) RunConsumer(queueName string, handler MsgHandler) {
	q, err := c.broker.channel.QueueDeclare(
		queueName,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		c.broker.logger.Panicf("failed to declare a queue:%s", err)
	}

	prefetchCount := 10
	if c.concurrency < 10 {
		prefetchCount = c.concurrency
	}

	if err = c.broker.channel.Qos(prefetchCount, 0, false); err != nil {
		c.broker.logger.Panicf("failed to declare a queue:%s", err)
	}

	messages, err := c.broker.channel.Consume(
		q.Name,
		"",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		c.broker.logger.Panicf("failed to register a consumer:%s", err)
	}

	for i := 0; i < c.concurrency; i++ {
		go func(msgs <-chan amqp.Delivery) {
			for msg := range messages {
				if err = handler.Handle(msg.Body); err != nil {
					c.broker.logger.Errorf("failed to handle message from queue:%s", err)
				}

				err = msg.Ack(false)
				if err != nil {
					c.broker.logger.Errorf("failed to acknowledge delivery:%s", err)
				}
			}
		}(messages)
	}

	c.broker.logger.Infof("Processing messages on %v goroutines", c.concurrency)

}
