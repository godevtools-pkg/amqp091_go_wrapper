package main

import (
	mb "gitlab.com/godevtools-pkg/amqp091_go_wrapper"
	"gitlab.com/godevtools-pkg/logging"
	"time"
)

const (
	queueName         = "country_urls"
	brokerUrl         = "amqp://guest:guest@rabbitmq:5672/"
	reconnectInterval = 5 * time.Second
)

func main() {
	logger := logging.GetLogger()
	broker, err := mb.NewBroker(logger, brokerUrl, reconnectInterval)
	if err != nil {
		logger.Errorf("Error while creating new broker:%s", err)
	}

	//Publishing
	if err = broker.Produce([]byte("{\"test\": 1}"), queueName); err != nil {
		logger.Errorf("Error while publishing message:%s", err)
	}
}
