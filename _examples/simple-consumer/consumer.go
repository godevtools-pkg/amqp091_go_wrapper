package main

import (
	mb "gitlab.com/godevtools-pkg/amqp091_go_wrapper"
	"gitlab.com/godevtools-pkg/logging"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const (
	queueName         = "country_urls"
	brokerUrl         = "amqp://guest:guest@rabbitmq:5672/"
	reconnectInterval = 5 * time.Second
	concurrency       = 100
)

func main() {
	logger := logging.GetLogger()
	broker, err := mb.NewBroker(logger, brokerUrl, reconnectInterval)
	if err != nil {
		logger.Errorf("Error while creating new broker:%s", err)
	}

	consumer := mb.NewConsumer(broker, concurrency)
	consumer.RunConsumer(queueName, queueHandler{})

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit
	logger.Print("Closing message broker")
	if err = broker.Close(); err != nil {
		logger.Errorf("error occurred on closing chanel and connection of a message broker: %s", err.Error())
	}

}

type queueHandler struct {
	logger logging.Logger
}

func (q queueHandler) Handle([]byte) error {
	q.logger.Infof("Handling message from a broker...")
	return nil
}
