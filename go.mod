module gitlab.com/godevtools-pkg/amqp091_go_wrapper

go 1.18

require (
	github.com/rabbitmq/amqp091-go v1.5.0
	gitlab.com/godevtools-pkg/logging v0.0.0-20221114172052-8b37838c9668
)

require (
	github.com/sirupsen/logrus v1.9.0 // indirect
	golang.org/x/sys v0.2.0 // indirect
)
